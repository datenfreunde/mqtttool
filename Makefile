SHELL := /bin/bash


TESTNETWORK := mqtt-test-network

PWD := $(shell pwd)

network: 
	test "$(TESTNETWORK)" = "$$(docker network ls --filter name=$(TESTNETWORK) --format '{{.Name}}')" || docker network create $(TESTNETWORK)

TCPPORT := 1883
WSPORT := 9001
NAME := mosquitto
mosquitto : network
	mkdir -p $(PWD)/mqtt-$(NAME) ;\
	test "$@" = "$$(docker ps --filter name='$(NAME)' --format '{{.Names}}')" || \
	docker run --restart=unless-stopped -d --name '$(NAME)' \
	       --network=$(TESTNETWORK) --network-alias='$(NAME)' \
		   --volume $(PWD)/mqtt-$(NAME):/mqtt/data/ \
	-p $(TCPPORT):1883 -p $(WSPORT):9001 toke/mosquitto

MONITOR := cat - 
monitor: mosquitto  
	echo MONITOR := '$(MONITOR)' ;\
	docker run --rm --network=$(TESTNETWORK) -it --name=$@ mqtttool 'mqttcat mqtt://mosquitto/%23 | $(MONITOR)'

heartbeat: mosquitto 
	docker run --rm --network=$(TESTNETWORK) -it --name=$@ mqtttool 'printf "tick\ntock\n" | mqttcat mqtt://mosquitto/heartbeat --loop --wait=1'

relay:
	docker run --rm --network=$(TESTNETWORK) -it --name=$@ mqtttool 'mqttcat mqtt://mosquitto/heartbeat | unbuffer -p grep "tock" | mqttcat --follow mqtt://mosquitto/tock'

build: 
	docker build -t mqtttool .



